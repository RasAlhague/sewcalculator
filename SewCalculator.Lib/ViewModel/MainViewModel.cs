﻿using SewCalculator.Lib.Model;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Data;

namespace SewCalculator.Lib.ViewModel
{
    public class MainViewModel : ViewModelBase
    {
        private ObservableCollection<SewCalculation> _calculations;
        private ListCollectionView _calculationsView;

        public ListCollectionView CalculationsView { get => _calculationsView; }

        public MainViewModel()
        {
            _calculations = new ObservableCollection<SewCalculation>();
            LoadCalculations(ref _calculations);
            _calculationsView = new ListCollectionView(_calculations);
        }

        public void LoadCalculations(ref ObservableCollection<SewCalculation> calculations)
        {
            if (File.Exists("./calcs.csv"))
            {
                string[] csvLines = File.ReadAllLines("./calcs.csv");

                foreach (var line in csvLines)
                {
                    SewCalculation sewCalculation = new SewCalculation();
                    sewCalculation.FromCsvLineString(line);

                    calculations.Add(sewCalculation);
                    sewCalculation.Recalculate();
                }
            }
        }
    }
}
