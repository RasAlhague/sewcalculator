﻿using SewCalculator.Lib.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SewCalculator.Lib.Model
{
    public class SewCalculation : ViewModelBase
    {
        private double _circumference;
        private double _multiplier;
        private double _extra;
        private string _name;
        private double _result;

        public double Circumference
        {
            get => _circumference;
            set
            {
                SetProperty(ref _circumference, value);
                Recalculate();
            }
        }
        public double Multiplier
        {
            get => _multiplier;
            set
            {
                SetProperty(ref _multiplier, value);
                Recalculate();
            }
        }
        public double Extra
        {
            get => _extra;
            set
            {
                SetProperty(ref _extra, value);
                Recalculate();
            }
        }
        public string Name { get => _name; set => SetProperty(ref _name, value); }
        public double Result { get => _result; set => SetProperty(ref _result, value); }

        public void Recalculate()
        {
            double value = (Circumference * Multiplier) + Extra;

            Result = value;
        }

        public void FromCsvLineString(string csvString)
        {
            string[] csvElements = csvString.Split(';');

            Name = csvElements[0];
            Circumference = double.Parse(csvElements[1]);
            Multiplier = double.Parse(csvElements[2]);
            Extra = double.Parse(csvElements[3]);
        }

        public string ToCsvLineString()
        {
            return $"{Name};{Circumference};{Multiplier};{Extra}";
        }
    }
}
